
const fs = require('fs');

/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
function readAndDeleteFiles() {
    fs.readFile(('lipsum_1.txt'),'utf8', (err ,data)=>{
        if (err) {
            console.log('file is not read 1');
        }else{
        console.log('file is readed');
        const uppercaseData = data.toUpperCase();
        fs.writeFile('newFile_1.txt',uppercaseData,(err,data)=>{
           if (err) {
            console.log("data not saved");
           } else {
            console.log("data saved");
            fs.readFile(('newFile_1.txt'),'utf8',(err,data)=>{
                if (err) {
                    console.log("file is not read");
                } else {
                    console.log("file is readed");
                    const lowercaseData = data.toLocaleLowerCase();
                    const splitData =JSON.stringify(lowercaseData.split("."));
                        fs.writeFile('newFile_2.txt',splitData,(err,data)=>{
                           if (err) {
                            console.log("file not created");
                           } else {
                            console.log("file is created");
                            fs.readFile('newFile_2.txt','utf8',(err,data) =>{
                                if (err) {
                                    console.log("file can be not read");
                                } else {
                                    console.log("file can be read");
                                    // console.log(data);
                                    const sortData =JSON.parse(data);
                                    const sortsData = JSON.stringify(sortData.sort());
                                    // console.log(sorts);
                                    fs.writeFile('newFile_3.txt',sortsData,(err,data) =>{
                                        if (err) {
                                            console.log("file is not created");
                                        } else {
                                            console.log("file is created");
                                            for (let index = 1; index < 4; index++) {
                                                fs.readFile(`newFile_${index}.txt`,'utf8',(err)=>{
                                                    if (err) {
                                                        console.log('file can not be read');
                                                    } else {
                                                        console.log('file can be read');
                                                        fs.unlink(`newFile_${index}.txt`,(err)=>{
                                                            if (err) {
                                                                console.log("file can not be deleted");
                                                            } else {
                                                                console.log("file is deleted");
                                                            }
                                                        })
                                                    }
                                                })
                                                
                                            }
                                        }
                                    })
                                }
                            })
                           }
                        })
                    }
            })
           }
        });
    }
    
    });
};
//  readAndDeleteFiles();
module.exports = readAndDeleteFiles;
