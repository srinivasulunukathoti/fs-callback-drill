const fs = require('fs');
// Using callbacks and the fs module's asynchronous functions, do the following:
// 1. Create a directory of random JSON files
// 2. Delete those files simultaneously.

function createDirectory(directory, callbacks) {
    fs.mkdir(directory,function(err){
        if (err) {
            console.log("directory is not created");
        } else {
            console.log("directory created successfully");
            callbacks();
        }
    });
};

function createRandomJson() {
    for (let index = 0; index < 6; index++) {
        fs.writeFile(`random${index}.json`,'console.log(srivasu)',function(err){
            if(err){
                console.error(err)
            }else {
                console.log("data saved");
                fs.unlink(`random${index}.json`,function (err) {
                    console.log(`random${index}.json deleted`);
                });
            }
        });
        
    }
};
module.exports = {createDirectory,createRandomJson};